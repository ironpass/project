var Star = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/lilstar.png');
	},
	hit : function(player) {
		if (player.getPositionY() >= this.getPositionY() - 30
				&& player.getPositionY() <= this.getPositionY() + 30 
				&& player.getPositionX() < this.getPositionX() + 30
				&& player.getPositionX() > this.getPositionX() - 30) {
			player.lives++;
			return true;
		}
		return false;
	}
});