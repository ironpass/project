var Nigga = cc.Sprite.extend({
	ctor : function() {
		this._super();
		this.initWithFile('res/images/lilnigg_right.png');
		this.vy = 0;
		this.vx = 0;
		this.freefall = true;
		this.direction = Nigga.DIR.LEFT;
		this.moveInterval = 0;
		this.moveLimit = Math.random()*20;
	},
	hit : function(player) {
		if (player.getPositionY() >= this.getPositionY() - 30
				&& player.getPositionY() <= this.getPositionY() + 30
				&& player.getPositionX() < this.getPositionX() + 30
				&& player.getPositionX() > this.getPositionX() - 30) {
			if (this.getPositionX()<player.getPositionX()) {
				player.jump();
				player.vy = 14;
				player.vx = 6;
				this.jump();
				this.vx = -20;
			} else {
				player.jump();
				player.vy = 14;
				player.vy = -6;
				this.jump();
				this.vx = 20;
			}
			return true;
		}
		return false;
	},
	update : function(dt) {
		if (this.getPositionY()>=700) {
			this.setPositionY(700);
		}
		if (this.getPositionX()>800) {
			this.setPositionX(0);
		}
		if (this.getPositionX()<0) {
			this.setPositionX(800);
		}
		if (this.getPositionY() < -100) {
			this.setPositionY(700);
			this.setPositionX(Math.random()*800);
			this.vy = 0;
			this.vx = 0;
		}
		if (this.freefall) {
			var pos = this.getPosition();
			this.setPosition(new cc.Point(pos.x + this.vx, pos.y + this.vy));
			if (this.vy>=-5) {
				this.vy += -0.5;
			} 
		} else {
			this.setPositionX(this.getPositionX() + this.vx);
		}
	},
	moveLeft : function() {
		this.vx = -5;
		this.setTexture('res/images/lilnigg_left.png');
		this.direction = Nigga.DIR.LEFT;
	},
	moveRight : function() {
		this.vx = 5;
		this.setTexture('res/images/lilnigg_right.png');
		this.direction = Nigga.DIR.RIGHT;
	},
	move : function() {
		if(this.moveInterval == 0) {
			this.moveInterval++;
			if (Math.random() < 0.5) {
				this.moveRight();
			} else {
				this.moveLeft();
			}
		} else {
			this.moveInterval++;
			if (this.direction == Nigga.DIR.LEFT) {
				this.moveLeft();
			} else {
				this.moveRight();
			}
		}
		if (this.moveInterval>this.moveLimit) {
			this.moveInterval=0;
		}
	},
	jump : function() {
		this.vy = 15;
		this.freefall=true;
	}
});
Nigga.DIR = {
		LEFT : 1,
		RIGHT : 2
	};