var res = {
    lilnugg_right_png : "res/images/lilnugg_right.png",
    lilnugg_left_png : "res/images/lilnugg_left.png",
    lilnigg_right_png : "res/images/lilnigg_right.png",
    lilnigg_left_png : "res/images/lilnigg_left.png",
    lilstar_png : "res/images/lilstar.png",
    lilboost_png : "res/images/lilboost.png",
    block_png : "res/images/block.png",
    rainbow_background_jpg : "res/images/rainbow_background.jpg",
    RainbowChickenDance_mp3 : "res/sound/RainbowChickenDance.mp3"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
