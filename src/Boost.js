var Boost = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/lilboost.png');
	},
	hit : function(player) {
		if (player.getPositionY() >= this.getPositionY() - 30
				&& player.getPositionY() <= this.getPositionY() + 30 
				&& player.getPositionX() < this.getPositionX() + 30
				&& player.getPositionX() > this.getPositionX() - 30) {
			if (player.speed < 10) {
				player.speed++;
			}
			return true;
		}
		return false;
	}
});