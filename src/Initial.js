var InitialLayer = cc.LayerColor.extend({
    init: function( ) {
        this._super( new cc.Color( 127, 127, 127, 255 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
     this.tutorial = cc.Sprite.create( 'res/images/tutorial.png' );
     this.tutorial.setPosition( new cc.Point( 400, 300 ) );
     this.addChild( this.tutorial );
     this.addKeyboardHandlers();
        
        
    },
    addKeyboardHandlers: function() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);
    },
    onKeyDown: function( keyCode, event ) {
        if( keyCode == cc.KEY.space ){
            cc.director.runScene(new StartScene());
        }
    },
    onKeyUp: function( keyCode, event ) {
    },
});
 
var InitialScene = cc.Scene.extend({
    onEnter: function() {
        console.log();
        this._super();
        var initialLayer = new InitialLayer();
        initialLayer.init();
        this.addChild( initialLayer );
    }
});