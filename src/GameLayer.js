var GameLayer = cc.LayerColor.extend({
	init : function() {
		this._super(new cc.Color(127, 127, 127, 255));
		this.setPosition(new cc.Point(0, 0));
        
		this.backStage();
		
		this.player = new Player();
		this.player.setPosition(new cc.Point(300, 400));
		this.addChild(this.player);
		this.player.scheduleUpdate();

		this.addKeyboardHandlers();
		this.scheduleUpdate();

		this.nigga = new Array();
		for (var i = 0; i < 8; i++) {
			this.nigga[i] = new Nigga();
		}
		for (var i = 0; i < this.nigga.length; i++) {
			this.nigga[i].setPositionX(Math.random() * 800);
			this.nigga[i].setPositionY(700);
			this.addChild(this.nigga[i]);
			this.nigga[i].scheduleUpdate();
		}
		this.block = [ new Block(), new Block(), new Block(), new Block() ];
		for (var i = 0; i < this.block.length; i++) {
			this.block[i].setPositionX(100 + (i * 200));
			this.block[i].setPositionY(80);
			this.addChild(this.block[i]);
		}

		this.star = new Star();
		this.addChild(this.star);
		this.star.setPositionY(900);
		this.starInterval = 0;
		
		this.boost = new Boost();
		this.addChild(this.boost);
		this.boost.setPositionY(900);
		this.boostInterval = 0;

		this.gameMessage();

		return true;
	},
	backStage : function() {
		this.background = new Background();
        this.background.setPosition( new cc.Point( 400, 300 ) );
        this.addChild( this.background );
        var audio = cc.audioEngine;
        audio.playEffect('res/sound/RainbowChickenDance.mp3', true);
	},
	update : function(dt) {
		this.starUpdate();
		this.boostUpdate();
		this.blockCollision();
		this.gameOverManager();
	},
	starUpdate : function() {
		this.starInterval++;
		if (this.starInterval >= 1000) {
			this.starInterval = 0;
			this.star.setPositionX(Math.random() * 800);
			this.star.setPositionY(400);
		}
		if (this.star.hit(this.player)) {
			this.star.setPositionY(900);
		}
	},
	boostUpdate : function() {
		this.boostInterval++;
		if (this.boostInterval >= 800) {
			this.boostInterval = 0;
			this.boost.setPositionX(Math.random() * 800);
			this.boost.setPositionY(400);
		}
		if (this.boost.hit(this.player)) {
			this.boost.setPositionY(900);
		}
	},
	blockCollision : function() {
		for (var k = 0; k < this.block.length; k++) {
			if (this.block[k].hit(this.player))
				break;
		}
		for (var i = 0; i < this.nigga.length; i++) {
			for (var j = 0; j < this.block.length; j++) {
				if (this.block[j].hit(this.nigga[i]))
					this.nigga[i].vy = 0;
			}
			if (this.nigga[i].hit(this.player)) {
				this.nigga[i].vy = 18;
			} else {
				this.nigga[i].move();
			}
		}
	},
	gameOverManager : function() {
		if (this.player.getPositionY() > -100
				&& this.player.getPositionY() < 600) {
			score++;
			this.scoreLabel.setString("Score: " + score);
			this.livesLabel.setString("Lives: " + this.player.lives);
		} else {
			this.player.lives--;
			if (this.player.lives < 0) {
				this.player.lives = -1;
				gameover = true;
				this.gameOverLabel.setString("    Game Over\nYour score is "
						+ score);
				this.restartLabel.setString("press SPACEBAR to restart!");
				this.scoreLabel.setString("");
				this.player.setPositionY(-500);
				this.livesLabel.setString("");
			} else {
				this.player.setPositionY(200);
				this.player.setPositionX(300);
				this.player.vy = 0;
				this.player.vx = 0;
			}
		}
	},
	restart : function() {
		this.player.lives++;
		this.player.speed = 2;
		gameover = false;
		score = 0;
		this.gameOverLabel.setString("");
		this.player.setPositionY(200);
		this.player.setPositionX(300);
		this.player.vy = 0;
		this.player.vx = 0;
		this.scoreLabel.setString("Score: " + score);
		this.livesLabel.setString("Lives: " + this.player.lives);
		this.restartLabel.setString("");
	},
	addKeyboardHandlers : function() {
		var self = this;
		cc.eventManager.addListener({
			event : cc.EventListener.KEYBOARD,
			onKeyPressed : function(keyCode, event) {
				self.onKeyDown(keyCode, event);
			},
			onKeyReleased : function(keyCode, event) {
				self.onKeyUp(keyCode, event);
			}
		}, this);
	},
	onKeyDown : function(keyCode, event) {
		if (keyCode == cc.KEY.up) {
			if (!this.player.freefall) {
				this.player.jump();
			}
		}
		if (keyCode == cc.KEY.right) {
			this.player.moveRight();
		}
		if (keyCode == cc.KEY.left) {
			this.player.moveLeft();
		}
		if (keyCode == cc.KEY.space && gameover == true) {
			this.restart();
		}
	},
	onKeyUp : function(keyCode, event) {
		if (keyCode != cc.KEY.up)
			this.player.vx = 0;
	},
	gameMessage : function() {
		this.scoreLabel = cc.LabelTTF.create('0', 'Arial', 40);
		this.scoreLabel.setPosition(new cc.Point(650, 550));
		this.addChild(this.scoreLabel);
		this.gameOverLabel = cc.LabelTTF.create('', 'Arial', 80);
		this.gameOverLabel.setPosition(new cc.Point(400, 400));
		this.addChild(this.gameOverLabel);
		this.livesLabel = cc.LabelTTF.create('0', 'Arial', 40);
		this.livesLabel.setPosition(new cc.Point(100, 550));
		this.addChild(this.livesLabel);
		this.restartLabel = cc.LabelTTF.create('', 'Arial', 30);
		this.restartLabel.setPosition(new cc.Point(400, 200));
		this.addChild(this.restartLabel);
	}
});

var StartScene = cc.Scene.extend({
	onEnter : function() {
		this._super();
		var layer = new GameLayer();
		layer.init();
		this.addChild(layer);
	}
});

var score = 0;
var gameover = false;
