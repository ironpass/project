var Player = cc.Sprite.extend({
	ctor: function() {
		this._super();
		this.initWithFile('res/images/lilnugg_right.png');
		this.vy = 0;
		this.vx = 0;
		this.freefall = true;
		this.lives = 0;
		this.speed = 2;
	},
	update : function(dt) {
		if (this.getPositionX()>800) {
			this.setPositionX(0);
		}
		if (this.getPositionX()<0) {
			this.setPositionX(800);
		}
		
		if (this.freefall) {
			var pos = this.getPosition();
			this.setPosition(new cc.Point(pos.x+this.vx, pos.y + this.vy));
			if (this.vy>=-5) {
				this.vy += -0.5;
			} 
		} else {
			this.setPositionX(this.getPositionX()+this.vx);
		}
	},
	jump: function() {
		this.vy = 15;
		this.freefall=true;
	},
	moveLeft: function() {
		this.vx = this.speed*(-1);
		this.setTexture('res/images/lilnugg_left.png');
	},
	moveRight: function() {
		this.vx = this.speed;
		this.setTexture('res/images/lilnugg_right.png');
	},
});