var Block = cc.Sprite.extend({
	ctor : function() {
		this._super();
		this.initWithFile('res/images/block.png');
	},
	hit : function(player) {
		if (player.getPositionY() >= this.getPositionY() + 54
				&& player.getPositionY() <= this.getPositionY() + 60 
				&& player.getPositionX() < this.getPositionX() + 55
				&& player.getPositionX() > this.getPositionX() - 55) {
			player.freefall = false;
			player.vy = 0;
			return true;
		}
		player.freefall = true;
		return false;
	}
});
